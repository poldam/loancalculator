<!DOCTYPE html>
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> КРЕДИТНЫЙ КАЛЬКУЛЯТОР by ESTROS (Russian Version)</title>
<meta name="description" content="Сравните банки и ежемесячные платежи-узнайте сколько составит Ваша переплата!">
<link rel="shortcut icon" href="favicon.ico">
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<style>
@import url(https://fonts.googleapis.com/css?family=Exo+2:400,700&subset=latin,cyrillic);
* { padding: 0; }
html {background-color: #efefef;}
body {font-family: 'Exo 2', sans-serif; margin: 60px; background-color:#fff;}
.center {text-align: center;}
.right { border: thin dotted #ccc; vertical-align: top; font-size: 12px; background-color: #efefef; padding-bottom: 30px; margin-bottom: 30px;}
input {padding:10px; border: 2px #ccc dotted; width: 100%; margin-bottom: 10px;}
select {padding:5px; border: 2px #ccc dotted; width: 100%; width: 100px;}
input[type="button"] { color: #fff; background-color: #333333; border: 0; width: 100px;}
.row{margin: 30px;}
hr {border-bottom: 2px dotted #383838;}
</style>

</head>

<body>
<a href="http://www.estros.gr"><img class="hvr-buzz-out" src="images/estros_big_gray.png" style="height:20px; position:absolute; left: 60px; top:20px;"></a>
<br>
<div class="row">
	<div class="col-md-12">
  	 <h2 class="text-center">
      Сравните банки и ежемесячные платежи и узнайте сколько составит Ваша переплата!  <br>
      <a href="en/" style="font-size: 18px;">English</a> | <strong style="font-size: 18px;">Русский</strong>
    </h2>
  </div>
</div>
<div class="row">
	<div class="col-md-5">
    <h3> Заполните форму!</h3>
    <form id="form" name="form" method="post">
    Название Банка<br>
    <input name="bank" id="bank"><br>
    <strong>*</strong> Сумма кредита ( без . или ,)<br>
    <input name="money" id="money"><br>
    Срок кредита<br>
    <input name="years" id="years"><br>
    1-ый год кредитования (%)<br>
    <input name="interest1" id="interest1"><br>
    <small style="font-style: italic; font-size: 10px;">Заполняется только, если процентная ставка по кредиту за 1-ый год отличается от ставок на последующие года кредитования</small><br><br>
    <strong>*</strong> Процентная ставка(%) <br>
    <input name="interest2" id="interest2"><br>
    Ваш желаемый ежемесячный платёж (заполняется без . или ,) <br>
    <input name="installment" id="installment"><br>
    <br>
    <input type="button" id="submit" name="submit" value="Посчитать">
    <br><br>
    </form>
  </div>
  <div class="col-md-5 right">
  	<h4> Результаты | Способ: <select id="mode"><option value="Append">Добавить</option><option value="Overwrite"> Переписать </option></select> | <input style="padding:7px; width: 80px;" type="button" id="clear" value="Стереть"></h4>
  	<div id="resultsContainer"></div>
  </div>
  <div class="col-md-2 text-right">
  	<?php
		 $from = 'EUR';
			$to = 'USD';
			$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from.$to.'=X';
			$handle = fopen($url, 'r');
			 
			if ($handle) {
					$result = fgetcsv($handle);
					fclose($handle);
			}
			$dollars = round($result[0], 4);
			
			$to = 'GBP';
			$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from.$to.'=X';
			$handle = fopen($url, 'r');
			 
			if ($handle) {
					$result = fgetcsv($handle);
					fclose($handle);
			}
			$pounds = round($result[0], 4);
			
			$to = 'JPY';
			$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from.$to.'=X';
			$handle = fopen($url, 'r');
			 
			if ($handle) {
					$result = fgetcsv($handle);
					fclose($handle);
			}
			$yens = round($result[0], 4);
			
			$to = 'RUB';
			$url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s='.$from.$to.'=X';
			$handle = fopen($url, 'r');
			 
			if ($handle) {
					$result = fgetcsv($handle);
					fclose($handle);
			}
			$rubles = round($result[0], 4);
			
			echo "<br><br>1&#8364; = ".$dollars."$<br>1&#8364; = ".$rubles."&#8381;<br>1&#8364; = ".$pounds."£<br> ";
		?>
     <!-- EXCHANGERATES.ORG.UK CHART WIDGET START -->
  	<br><br>
    <div style="width:178px;margin:0 auto; margin-bottom:30px;border:1px solid #222222;background:#fff; float: right;">
    <div style="width:174px;text-align:center;margin:0 auto;padding:2px 0px;background:#222222;font-family:;font-size:11px;color:#FFFFFF;font-weight:bold;">
    <a style="color:#FFFFFF;text-decoration:none;text-transform:uppercase;" href="http://www.exchangerates.org.uk/Euros-to-Russian-Roubles-currency-conversion-page.html" target="_new" title="EUR RUB">EUR RUB</a> CHARTS
    </div>
    <div style="padding:2px;">
    <script type="text/javascript">	
	    var dcf = 'EUR';
	    var dct = 'RUB';
	    var mcol = '222222';
	    var mbg = 'fff';
	    var tc = 'FFFFFF';
	    var tz = 'userset';
    
    </script><script type="text/javascript" src="http://www.currency.me.uk/remote/ER-CHART-1.php"></script></div>
    </div>
    <!-- EXCHANGERATES.ORG.UK CHART WIDGET END -->
  </div>
</div>

<script>
$('#clear').on('click', function(){
	$('#resultsContainer').html('');	
});

$('#submit').on('click', function(){
	var data = $('#form').serializeArray();
	$.ajax({
		url			: "scripts/calculate.php",
		type		: "POST",
		data		: {data: data},
		success	: function (data) {
			var prev = '';
			if($('#mode').val() == 'Append')
				prev = $('#resultsContainer').html();
			$('#resultsContainer').html(data + "" + prev);			
		},
		error		: function (errorThrown) {
			alert('Error while calculating..' + errorThrown);
		}
	});
});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57355277-2', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>