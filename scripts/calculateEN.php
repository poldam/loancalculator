<?php

function calcPmt($amt, $i, $term ) 
{
	$int 	= $i / 1200;
	$int1 = 1 + $int;
	$r1 	= pow($int1, $term);
	
	$pmt 	= $amt * ($int * $r1) / ($r1 - 1);
	return $pmt;
}

$extramoney = 0;

$data = $_POST['data'];

if($data[1]['value'] != "" && $data[4]['value'] != "")
{
	if($data[0]['value'] != "")
		$bank = $data[0]['value'];
	else
		$bank = "Test Bank";
	$money = $data[1]['value'];
	
	if($data[2]['value'] != "")
		$years = $data[2]['value'];
	else
		$years = 0;
	
	if($data[3]['value'] == "")
		$interest1 = $data[4]['value'];
	else
		$interest1 = $data[3]['value'];
		
	$interest2 = $data[4]['value'];
	
	if($data[5]['value'] != "")
		$installment = $data[5]['value'];
	else
		$installment = 0;
	
	$toreturn = "<h2>".$bank."</h2>";
	
	if($years != 0)
		$toreturn .= "<strong>MONEY:</strong> ".$money." for ".$years." years OR ".($years * 12)." months<br>";
	else
		$toreturn .= "<strong>MONEY:</strong> ".$money."<br>";
		
	$toreturn .= "<strong>RATE (1st Year):</strong> ".$interest1."%<br>";
	$toreturn .= "<strong>RATE:</strong> ".$interest2."%<br>";
	
	if($years != 0)
	{
		$banksugg = ( calcPmt($money, $interest1, $years * 12) * (12)/($years * 12)) + ( calcPmt($money, $interest2, $years * 12) * ($years * 12 - 12)/($years * 12));
		
		$toreturn .= "<strong>BANK INSTALLMENT:</strong> ".round($banksugg)."<br>";
		$toreturn .= "<br><br><div style='width: auto; padding:15px; color:#fff; background-color:#333333; font-size: 16px;'>Following the bank's suggestion (minimum monthly payment), after ".$years." years of paying you will have paid in total: <strong>".round(($years*12)*$banksugg)."</strong> and overpaid: <strong>".round(($years*12)*$banksugg - $money)."</strong></div><br><br>";
	}
	else
		$toreturn .= "<strong>BANK INSTALLMENT:</strong> N/A <br>";
																														
	if($installment != "")
	{
		$neverpay = 0;
		if($money*$interest1/100 > 14*$installment)
			$neverpay = 1;
		
		if($neverpay == 0)
		{
			$toreturn .= "<h2>DESIRED MONTHLY PAYMENT:  ".$installment."</h2>
			<h3>Based on your desired monthly payment</h3>";
			
			$firstyear = 0;
			if($interest1 != '')
				$firstyear = $money + ($money * $interest1)/100;
			else
				$firstyear = $money + ($money * $interest2)/100;
			
			$toreturn .= "<strong>(1st Year) YOU OWE:</strong> ".$firstyear." | <strong>YOU PAY EXTRA:</strong> ".round($firstyear - $money)."<br>";
			
			$extramoney = $firstyear - $money;
			
			$moneyleft = $firstyear - ($installment * 12);
			
			$toreturn .= "AFTER the 1st YEAR you have paid: <strong>".round($installment * 12)."</strong> and you have left: <strong>".round($moneyleft)."</strong><br><br>";
			
			$k = 2;
			$manyyears = 0;
			for($i = $moneyleft; $i >= 0; $i = ($i -($installment * 12)))
			{
				if($k == 2)
					$ending = "nd";
				else if($k == 3)
					$ending = "rd";
				else
					$ending = "th";
				$newowe = $i + ($i * $interest2)/100;
				$toreturn .= "<strong>(".$k.$ending." Year) YOU OWE:</strong> ".round($newowe)." | <strong>YOU PAY EXTRA:</strong> ".round($newowe - $i)."<br>";
				if(($newowe - (12 * $installment)) >= 0)
					$toreturn .= "AFTER the ".$k.$ending." YEAR you have paid: <strong>".round($installment * 12 * $k)."</strong> and you have left: <strong>".round($newowe - (12 * $installment))."</strong><br><br>";
				else
				{
					$tor = ($newowe - (12 * $installment));
					$toreturn .= "AFTER the ".$k.$ending." YEAR you have paid: <strong>".round($installment * 12 * $k + $tor)."</strong> and you have left: <strong>0</strong><br><br>";
				}
				$extramoney = $extramoney + $newowe - $i;
				
				$i = $newowe;
				
				$k++;
				if($k == 32)
				{
					$manyyears = 1;
					break;
				}
			}
			if($manyyears == 1)
				$toreturn .= "<br><div style='width: auto; padding:15px; color:#fff; background-color: red; font-size: 16px;'><strong>WITH YOUR DESIRED MONTHLY PAYMENT YOU WILL NOT HAVE PAID BACK YOUR LOAN IN 30 YEARS</strong></div><br><br><br>";
			else
				$toreturn .= "<br><div style='width: auto; padding:15px; color:#fff; background-color:#333333; font-size: 16px;'>WITH THE DESIRED MONTHLY PAYMENT you overpay: <strong style='font-weight: bold; color: red;'>".round($extramoney)."</strong> and you pay back in about <strong>".($k-1)." years</strong></div>";
		}
		else
		{
			$toreturn .= "<br><div style='width: auto; padding:15px; color:#fff; background-color: red; font-size: 16px;'><strong>YOU WILL NEVER BE ABLE TO PAY BACK YOUR LOAN WITH YOUR CURRENT DESIRED MONTHLY PAYMENT</strong></div><br><br><br>";
		}
	}
	$toreturn .= "<hr><br>";
}
else
	$toreturn = "<br>Fill in the form properly! Fields with '*' are required for the calculations to start!<br>";
echo $toreturn;
?>