<?php

function calcPmt($amt, $i, $term ) 
{
	$int 	= $i / 1200;
	$int1 = 1 + $int;
	$r1 	= pow($int1, $term);
	
	$pmt 	= $amt * ($int * $r1) / ($r1 - 1);
	return $pmt;
}

$extramoney = 0;

$data = $_POST['data'];

if($data[1]['value'] != "" && $data[4]['value'] != "")
{
	if($data[0]['value'] != "")
		$bank = $data[0]['value'];
	else
		$bank = "Test Bank";
	$money = $data[1]['value'];
	
	if($data[2]['value'] != "")
		$years = $data[2]['value'];
	else
		$years = 0;
	
	if($data[3]['value'] == "")
		$interest1 = $data[4]['value'];
	else
		$interest1 = $data[3]['value'];
		
	$interest2 = $data[4]['value'];
	
	if($data[5]['value'] != "")
		$installment = $data[5]['value'];
	else
		$installment = 0;
	
	$toreturn = "<h2>".$bank."</h2>";
	
	if($years != 0)
		$toreturn .= "<strong>Сумма кредита:</strong> ".$money." на ".$years." год/года/лет или ".($years * 12)." месяц/месяца/месяцев<br>";
	else
		$toreturn .= "<strong>Сумма кредита:</strong> ".$money."<br>";
		
	$toreturn .= "<strong>Процент (Первый год):</strong> ".$interest1."%<br>";
	$toreturn .= "<strong>Процент:</strong> ".$interest2."%<br>";
	
	if($years != 0)
	{
		$banksugg = ( calcPmt($money, $interest1, $years * 12) * (12)/($years * 12)) + ( calcPmt($money, $interest2, $years * 12) * ($years * 12 - 12)/($years * 12));
		
		$toreturn .= "<strong>ЕЖЕМЕСЯЧНЫЙ ПЛАТЁЖ:</strong> ".round($banksugg)."<br>";
		$toreturn .= "<br><br><div style='width: auto; padding:15px; color:#fff; background-color:#333333; font-size: 16px;'> По предложению банка ( минимальный ежемесячный платёж), после  ".$years." лет кредитования составит : <strong>".round(($years*12)*$banksugg)."</strong> и переплата будет: <strong>".round(($years*12)*$banksugg - $money)."</strong></div><br><br>";
	}
	else
		$toreturn .= "<strong>ЕЖЕМЕСЯЧНЫЙ ПЛАТЁЖ:</strong> N/A <br>";
																														
	if($installment != "")
	{
		$neverpay = 0;
		if($money*$interest1/100 > 14*$installment)
			$neverpay = 1;
		
		if($neverpay == 0)
		{
			$toreturn .= "<h2>ЖЕЛАЕМЫЙ ЕЖЕМЕСЯЧНЫЙ ПЛАТЁЖ:  ".$installment."</h2>
			<h3>Исходя из Вашего желаемого ежемесячного платежа</h3>";
			
			$firstyear = 0;
			if($interest1 != '')
				$firstyear = $money + ($money * $interest1)/100;
			else
				$firstyear = $money + ($money * $interest2)/100;
			
			$toreturn .= "<strong>(1-ый год) ВЫ ДОЛЖНЫ:</strong> ".$firstyear." | <strong>ВЫ ПЕРЕПЛАЧИВАЕТЕ:</strong> ".round($firstyear - $money)."<br>";
			
			$extramoney = $firstyear - $money;
			
			$moneyleft = $firstyear - ($installment * 12);
			
			$toreturn .= "ПОСЛЕ 1-го года Вы заплатите: <strong>".round($installment * 12)."</strong> и Вам останется выплатить: <strong>".round($moneyleft)."</strong><br><br>";
			
			$k = 2;
			$manyyears = 0;
			for($i = $moneyleft; $i >= 0; $i = ($i -($installment * 12)))
			{
				if($k == 2)
					$ending = "nd";
				else if($k == 3)
					$ending = "rd";
				else
					$ending = "th";
				$ending = "";
				$newowe = $i + ($i * $interest2)/100;
				$toreturn .= "<strong>(".$k.$ending." Год) ВЫ ДОЛЖНЫ:</strong> ".round($newowe)." | <strong>ВЫ ПЕРЕПЛАЧИВАЕТЕ:</strong> ".round($newowe - $i)."<br>";
				if(($newowe - (12 * $installment)) >= 0)
					$toreturn .= "ПОСЛЕ ".$k.$ending." ГОДА Вы заплатите: <strong>".round($installment * 12 * $k)."</strong> и Вам останется выплатить: <strong>".round($newowe - (12 * $installment))."</strong><br><br>";
				else
				{
					$tor = ($newowe - (12 * $installment));
					$toreturn .= "ПОСЛЕ ".$k.$ending." ГОДА Вы заплатите: <strong>".round($installment * 12 * $k + $tor)."</strong> и Вам останется выплатить : <strong>0</strong><br><br>";
				}
				$extramoney = $extramoney + $newowe - $i;
				
				$i = $newowe;
				
				$k++;
				if($k == 32)
				{
					$manyyears = 1;
					break;
				}
			}
			if($manyyears == 1)
				$toreturn .= "<br><div style='width: auto; padding:15px; color:#fff; background-color: red; font-size: 16px;'><strong>С ВАШИМ ЖЕЛАЕМЫМ ЕЖЕМЕСЯЧНЫМ ПЛАТЕЖОМ ВЫ НЕ ВЫПЛАТИТЕ КРЕДИТ ЗА 30 ЛЕТ</strong></div><br><br><br>";
			else
				$toreturn .= "<br><div style='width: auto; padding:15px; color:#fff; background-color:#333333; font-size: 16px;'>С ВАШИМ ЖЕЛАЕМЫМ ЕЖЕМЕСЯЧНЫМ ПЛАТЕЖОМ переплата составит : <strong style='font-weight: bold; color: red;'>".round($extramoney)."</strong> и Вы выплатите всё через <strong>".($k-1)." год/года/лет</strong></div>";
		}
		else
		{
			$toreturn .= "<br><div style='width: auto; padding:15px; color:#fff; background-color: red; font-size: 16px;'><strong>ВЫ НИКОГДА НЕ СМОЖИТЕ ВЫПЛАТИТЬ КРЕДИТ С ВАШИМ ЖЕЛАЕМЫМ ЕЖЕМЕСЯЧНЫМ ПЛАТЕЖОМ</strong></div><br><br><br>";
		}
	}
	$toreturn .= "<hr><br>";
}
else
	$toreturn = "<br>Заполните форму правильно! Поля со знаком '*' необходимы для начала расчетов!<br>";
echo $toreturn;
?>